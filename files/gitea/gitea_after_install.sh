#!/bin/sh
chown root:git /etc/gitea
chmod 750 /etc/gitea
chmod 644 /etc/gitea/app.ini
rm /tmp/gitea_after_install.sh
